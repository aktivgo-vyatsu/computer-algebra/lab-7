from prime.prime import *

if __name__ == '__main__':
    print('Решето эратосфена:', eratosthenes_sieve(100))

    n = int(input('Введите n: '))

    print('Пробные деления:', test_divisions(n))
    print('Критерий Вилсона:', wilson_criterion(n))
    print('Критерий Лукаса:', lucas_criterion(n, 100000))
