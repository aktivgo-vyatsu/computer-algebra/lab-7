import math


def eratosthenes_sieve(n: int) -> list:
    set = [1]
    for i in range(3, n + 1):
        set.append(0 if i % 2 == 0 else 1)

    for i in range(3, math.ceil(math.sqrt(n)) + 1):
        current = i
        if set[i - 2] == 0:
            continue
        for j in range(i + 1, n + 1):
            if set[j - 2] == 0:
                continue
            if j % current == 0:
                set[j - 2] = 0

    result = []
    for i in range(len(set)):
        if set[i] == 0:
            continue
        result.append(i + 2)

    return result


def test_divisions(n: int) -> bool:
    for i in range(2, math.ceil(math.sqrt(n)) + 1):
        if n % i == 0:
            return False

    return True


def wilson_criterion(n: int) -> bool:
    return (math.factorial(n - 1) + 1) % n == 0


def lucas_criterion(n: int, iterations: int) -> bool:
    qs = __prime_divisors(n - 1)

    for q in qs:
        a = 1
        while a <= iterations and (pow(a, n - 1) % n != 1 or pow(a, int((n - 1) / q)) % n == 1):
            a += 1
        if a == iterations + 1:
            return False

    return True


def __prime_divisors(n: int) -> list:
    result = []
    for prime in eratosthenes_sieve(n):
        if n % prime == 0:
            result.append(prime)
    return result
